<?php
// connection to database
include '../modele/connexion.php';
session_start();
$user= $_SESSION['user'];





// we retrieve the data from the form
$name= htmlspecialchars($_POST['name']);
$description= htmlspecialchars($_POST['description']);
$startdate= htmlspecialchars($_POST['startdate']);
$enddate= htmlspecialchars($_POST['enddate']);
$nb_place= htmlspecialchars($_POST['nb_place']);
$organizer_id=$user['id'];

// we prepare the query and bind parameters
$query = "INSERT INTO events (name, description, startdate, enddate, organizer_id, nb_place) VALUES (:name,:description,:startdate,:enddate,:organizer_id,:nb_place )";
$req = $bdd->prepare($query);

$req->bindValue(':name', $name, PDO::PARAM_STR);
$req->bindValue(':description', $description, PDO::PARAM_STR);

$req->bindValue(':startdate', $startdate, PDO::PARAM_STR);

$req->bindValue(':enddate', $enddate, PDO::PARAM_STR);
$req->bindValue(':nb_place', $nb_place, PDO::PARAM_INT);
$req->bindValue(':organizer_id',$organizer_id , PDO::PARAM_INT);


// we execute the query
$rep = $req->execute();


// we verify if the event is added
if ($rep) {
  $_SESSION['addEventMessage'] = "Event added";
  header('location:../vue/calendar.php') ;
}
else {
  $_SESSION['addEventMessage'] = "Event not added";
header('location:../vue/calendar.php') ;
}

 ?>
