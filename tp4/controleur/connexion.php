<?php
session_start();
include '../modele/connexion.php';

// we retrieve the data from user inputs
$login = htmlspecialchars($_POST['login']);
$passwordFromUser = htmlspecialchars($_POST['password']);

// we prepare the query and bind parameters
$query = "SELECT * FROM Users WHERE login= :login";
$statement = $bdd ->prepare($query);
$statement->bindValue(':login', $login, PDO::PARAM_STR);

// we execute the query and fetch the first row
$statement->execute();
$row = $statement->fetch(PDO::FETCH_ASSOC);

// we get the password and the rank from the database
$passwordFromBDD = $row['password'];
$rank = $row['rank'];

// we verify if the password is correct by comparing
// the hash from the database with the user input
// then we verify the rank of the user
$passISOK = password_verify($passwordFromUser,$passwordFromBDD);
if($passISOK)
{
  $_SESSION['user'] = $row;
  header('location:../vue/calendar.php') ;
}
else {
  $_SESSION['error'] = "Wrong login and/or password";
  header('location:../vue/connexion.php') ;
}




 ?>
