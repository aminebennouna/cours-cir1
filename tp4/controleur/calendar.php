<?php

session_start();



/* we get the five events of a day
 * @param date $day
 * @param date $month
 * @param date $year
 *@param  $dayEvents is the calendar whith the current month and the current year
 * @return $text , calendar with the events
 */
function getEventsOfADay($day,$month,$year,$dayEvents){
  $text = "<ul>" . $text . $day ;
  $i = 0;
  foreach ($dayEvents as $de) {
    foreach ($de as $key => $value) {
      if($key==$day){
        $i++;
        if($i <=5){
          $text =   $text ."<li>" . $value['name'] . "</li>";
        } else if( $i == 6) {
          $text = $text ."<li> <a href='eventsOfADay.php?day=".$day."&month=".$month."&year=".$year."' >voir la suite</a> </li>";
        }
      }
    }
  }
  $text =   $text ."</ul>";
  return $text;
}


/* we get all events of a day
 * @param date $day
 * @param date $month
 * @param date $year
 * @return $text , calendar with all events
 */
 function getAllEventsOfADay($day,$month,$year){
  $dayEvents = loadCalendar($month, $year);
  $text = "<ul>" . $text . $day . '/' . $month .'/'. $year ;
  foreach ($dayEvents as $de) {
    foreach ($de as $key => $value) {
      if($key==$day){
        $text = $text ."<li>" . $value['name'] . "</li>";
      }
    }
  }
  $text =   $text ."</ul>";
  return $text;
}
/* we get the events in a calendar
 * @param date $actualMonth
 * @param date $actualYear
 * @return $dayEvents , calendar with all events in the current month
 */
function loadCalendar($actualMonth, $actualYear)
{
  include '../modele/connexion.php';
  $dayEvents = array();

// we verify user rank , if the rank is ORGANIZER we select his events , else we select all events
  $user=$_SESSION['user'];

  $rank = $user['rank'];

  if($rank == "ORGANIZER"){
    $organizer_id= $user['id'];

// we prepare the query and bind parameter
    $query = "SELECT * FROM events WHERE organizer_id= :organizer_id";
    $statement = $bdd ->prepare($query);
    $statement->bindValue(':organizer_id', $organizer_id, PDO::PARAM_INT);
  } else {
    $query = "SELECT * FROM events ";
    $statement = $bdd ->prepare($query);
  }
// we execute the query

  $statement->execute();


// we go through the calendar and we create $dayEvent
  while ($event = $statement->fetch()) {
    $begin = new DateTime($event['startdate']);
    $end = new DateTime($event['enddate']);
    $end = $end->modify( '+1 day' );
    $interval = DateInterval::createFromDateString('1 day');
    $period = new DatePeriod($begin, $interval, $end);
    foreach ($period as $dt) {
        $day = $dt->format("d");
        $y = $dt->format("Y");
        $m = $dt->format("m");
        if($y == $actualYear && $m == $actualMonth){
          $dayEvent = array(
            $day  => $event
          );
// we add $dayEvent in $dayEvents
          array_push($dayEvents,$dayEvent);
        }
    }
  }
  return $dayEvents;
}




 ?>
