<?php

include 'header.php';
session_start();

$user =  $_SESSION['user'];

if(!isset($user) || $user['rank'] != "ORGANIZER"){
    $_SESSION['error'] = "Please connect";
    header('location:connexion.php') ;
}

?>

<div class="container text-center mt-5">


    <h1>Ajouter un événement</h1>
    <form class="mt-5" method="post" action="../controleur/addEvents.php">
        <input type="text" id="name" name="name" class="form-control" placeholder="Nom de l'événement" required>
        <br>
        <textarea id="description" name="description" class="form-control" placeholder="Description de l'événement" required></textarea>
        <br>
        <div >
            <div >
                <label for="nb_place">Nombre de places</label>
            </div>
            <div >
                <input type="number" id="nb_place" name="nb_place" class="form-control" value="10" required>
            </div>
        </div>
        <br>
        <div >
            <div >
                <label for="startdate">Début de l'événement</label>
            </div>
            <div >
                <input type="date" id="startdate" name="startdate" class="form-control" >
            </div>
        </div>
        <br>
        <div >
            <div >
                <label for="enddate">Fin de l'événement</label>
            </div>
            <div >
                <input type="date" id="enddate" name="enddate" class="form-control" >
            </div>
        </div>
        <br>
        <input type="submit" class="btn btn-lg btn-primary btn-block" value="Créer l'événement">
    </form>
</div>
